# Setup Info

This runs on port 7777. To create a room for 8 people, you can set up port forwarding for this port, and then 7 other people will be able to join your room using your IP.

Once the room is created, the host of the room can start the simulation by specifying a mechanic file (e.g. `Mechanics/HeavensfallDemo.json`) and then pressing the start button.

# Notes

- Some settings can be adjusted via the settings menu or directly editing the settings json.
- A default action set is defined in the DefaultActionSet.json, which specifies the properties of the abilities that the different classes have
